const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(2)
    .max(25),

  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required(),

  name: Joi.string()
    .alphanum()
    .min(2)
    .max(25),

  role: Joi.string()
    .uppercase()
    .valid('SHIPPER', 'DRIVER')
    .required(),

  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
});

const User = mongoose.model('User', {
  name: {
    type: String,
    required: false,
  },
  username: {
    type: String,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
  },
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
});

module.exports = {
  User,
  userJoiSchema,
};
