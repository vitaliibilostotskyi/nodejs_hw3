const mongoose = require('mongoose');
const Joi = require('joi');

const truckTypesDem = [
  {
    name: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    weight: 1700,
  },
  {
    name: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    weight: 2500,
  },
  {
    name: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    weight: 4000,
  },
];

const truckJoiSchema = Joi.object({
  type: Joi.string()
    .uppercase()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
    .required(),
});

const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
  },
  type: {
    type: String,
    default: false,
  },
  status: {
    type: String,
    default: 'IS',
    required: true,
  },
  created_date: {
    type: String,
  },
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = {
  Truck,
  truckJoiSchema,
  truckTypesDem,
};
