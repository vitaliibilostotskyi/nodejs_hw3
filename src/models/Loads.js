const mongoose = require('mongoose');
const Joi = require('joi');

const loadStatuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStates = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to Delivery', 'Arrived to Delivery'];

const loadJoiSchema = Joi.object({

  name: Joi.string()
    .min(2)
    .max(50),

  payload: Joi.number()
    .min(1)
    .max(100000),

  pickup_address: Joi.string()
    .min(20)
    .max(100),

  delivery_address: Joi.string()
    .min(20)
    .max(100),

  dimensions: Joi.object({
    width: Joi.number().min(1).max(700).integer(),
    length: Joi.number().min(1).max(700).integer(),
    height: Joi.number().min(1).max(700).integer(),
  }),

});

const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
  },
  state: {
    type: String,
    default: 'En route to Pick Up',
  },
  status: {
    type: String,
    default: 'NEW',
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },
  logs: {
    type: Array,
  },
  created_date: {
    type: String,
  },
});

const Load = mongoose.model('load', loadSchema);

module.exports = {
  Load,
  loadJoiSchema,
  loadStatuses,
  loadStates,
};
