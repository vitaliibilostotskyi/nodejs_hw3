const { Truck, truckJoiSchema } = require('../models/Trucks');
const { saveTruck } = require('../services/trucksService');

const addUserTruck = async (req, res) => {
  const { type } = req.body;
  await truckJoiSchema.validateAsync({ type });
  if (req.user.role === 'DRIVER') {
    await saveTruck(type, req);
    return res.status(200).json({ message: 'Truck created successfully' });
  }
  return res.status(400).json({ message: 'Only for drivers' });
};

const getUserTrucks = async (req, res) => {
  const trucks = await Truck.find({ created_by: req.user.userId });
  if (trucks.length === 0) {
    return res.status(400).json({ message: 'No trucks found' });
  } else {
    return res.status(200).json({
      trucks,
    });
  }
};

const getUserTruckById = async (req, res) => {
  const truck = await Truck.findOne({ _id: req.params.id, created_by: req.user.userId });
  if (!truck) {
    res.status(400).json({ message: 'Access denied' });
  } else {
    res.status(200).json({ truck });
  }
};

const updateUserTruckById = async (req, res) => {
  const { type } = req.body;
  await truckJoiSchema.validateAsync({ type });
  return Truck.findOneAndUpdate(
    { _id: req.params.id, created_by: req.user.userId, assigned_to: null },
    { $set: { type } },
  )
    .then((result) => {
      if (!result) {
        return res.status(400).json({ message: 'Access denied' });
      }
      return res.status(200).json({ message: 'Truck details changed successfully' });
    });
};

const assignUserTruckById = async (req, res) => {
  const assignedTruck = await Truck.findOne({ assigned_to: req.user.userId });

  if (assignedTruck) {
    assignedTruck.assigned_to = null;
    await assignedTruck.save();
  }

  const truck = await Truck.findOne({
    _id: req.params.id,
    created_by: req.user.userId,
    assigned_to: null,
  });
  if (!truck) {
    return res.status(400).json({ message: 'truck is not found' });
  }
  truck.assigned_to = req.user.userId;
  await truck.save();
  return res.status(200).json({ message: 'Truck assigned successfully' });
};

const deleteUserTruckById = async (req, res) => {
  const truck = await Truck.findOne({
    _id: req.params.id,
    userId: req.user.userId,
    assigned_to: null,
  });
  if (!truck) {
    return res.status(400).json({ message: 'Truck is assinged' });
  }
  truck.deleteOne()
    .then(() => {
      return res.status(200).json({ message: 'Truck deleted successfully' })
    });
};

module.exports = {
  addUserTruck,
  getUserTruckById,
  deleteUserTruckById,
  getUserTrucks,
  updateUserTruckById,
  assignUserTruckById,
};
