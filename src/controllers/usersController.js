require('dotenv').config();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../models/Users');
const { saveUser, savePass } = require('../services/usersService');

const createProfile = async (req, res) => {
  const {
    name, username, password, role, email,
  } = req.body;
  await userJoiSchema.validateAsync({
    name, username, password, role, email,
  });
  await saveUser({
    name, username, password, role, email,
  });
  return res.status(200).json({
    message: 'Profile created successfully',
  });
};

const login = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, userId: user.id, role: user.role };
    const jwtToken = jwt.sign(payload, process.env.SECURE_PASS);
    return res.status(200).json({ jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const getProfileInfo = async (req, res) => {
  const user = await User.findById({ _id: req.user.userId });
  return res.status(200).send({
    user: {
      _id: user.id,
      role: user.role,
      email: user.email,
      created_date: user.created_date,
    },
  });
};

const deleteUser = async (req, res) => {
  await User.findByIdAndDelete({ _id: req.user.userId })
    .then(() => res.status(200).json({ message: 'Profile deleted successfully' }));
};

const changePass = async (req, res) => {
  const { newPassword, oldPassword } = req.body;
  const user = await User.findById({ _id: req.user.userId });
  if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
    await savePass(user, newPassword);
    return res.status(200).json({ message: 'Password changed successfully' });
  }
  return res.status(400).json({ message: 'Wrong password' });
};

module.exports = {
  createProfile,
  login,
  getProfileInfo,
  deleteUser,
  changePass,
};
