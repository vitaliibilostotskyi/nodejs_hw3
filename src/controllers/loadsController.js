const { Load, loadJoiSchema } = require('../models/Loads');
const { Truck, truckTypesDem } = require('../models/Trucks');
const { saveLoad, updateLoad, deleteLoad, postLoad, assignLoad } = require('../services/loadsService');

const addUserLoad = async (req, res) => {
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  await loadJoiSchema.validateAsync({
    name, payload, pickup_address, delivery_address, dimensions,
  });
  if (req.user.role == 'SHIPPER') {
    await saveLoad({
      name, payload, pickup_address, delivery_address, dimensions, req,
    });
    return res.status(200).json({ message: 'Load created successfully' });
  }
  return res.status(400).json({ message: 'Only for shippers' });
};

const getUserLoads = async (req, res) => {
  const { status, limit = 10, offset = 0 } = req.query;
  let loads;
  if (req.user.role === 'SHIPPER') {
    if (status) {
      loads = await Load.find({ created_by: req.user.userId, status }).skip(offset).limit(limit);
    } else {
      loads = await Load.find({ created_by: req.user.userId }).skip(offset).limit(limit);
    }
    if (loads.length === 0) {
      return res.status(400).json({ message: 'No loads found' });
    }
    return res.status(200).json({
      loads
    });
  }
  if (req.user.role === 'DRIVER') {
    if (status) {
      loads = await Load.find({ assigned_to: req.user.userId, status }).skip(offset).limit(limit);
    } else {
      loads = await Load.find({ assigned_to: req.user.userId }).skip(offset).limit(limit);
    }
    if (loads.length === 0) {
      return res.status(400).json({ message: 'No loads found' });
    }
    return res.status(200).json({
      loads: {
        loads,
      },
    });
  }
};

const getUserLoadById = async (req, res) => {
  const load = await Load.findOne({ _id: req.params.id, created_by: req.user.userId });
  if (!load) {
    return res.status(400).json({ message: 'Access denied' });
  }
  return res.status(200).json({ load });
};

const updateUserLoadById = async (req, res) => {
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  await loadJoiSchema.validateAsync({
    name, payload, pickup_address, delivery_address, dimensions,
  });
  const load = await Load.findOne({ _id: req.params.id, created_by: req.user.userId, status: 'NEW' })
  if (!load) {
    return res.status(400).json({ message: 'Access denied' });
  }
  await updateLoad({name, payload, pickup_address, delivery_address, dimensions,load})
  return res.status(200).json({ message: 'Load details changed successfully' });
};

const deleteUserLoadById = async (req, res) => {
  const load = await Load.findOne({
    _id: req.params.id,
    created_by: req.user.userId,
    status: 'NEW',
  });
  if (!load) {
    return res.status(400).json({ message: 'Load is undefined' });
  }
  deleteLoad(load)
  return res.status(200).json({ message: 'Load deleted successfully' })
};

const postUserLoadById = async (req, res) => {
  const load = await Load.findOne({
    _id: req.params.id,
    created_by: req.user.userId,
    assigned_to: null,
  });
  if (!load) {
    return res.status(400).json({ message: 'load is not found' });
  }
  await postLoad(load)
  const suitableTruckType = truckTypesDem.filter((item) => {
    return item.width >= load.dimensions.width
      && item.length >= load.dimensions.length
      && item.height >= load.dimensions.height
      && item.weight >= load.payload});
  const truck = await Truck.findOne({
    status: 'IS',
    assigned_to: { $ne: null },
    $or: [{ type: suitableTruckType[0].name },
      { type: suitableTruckType[1].name },
    ],
  });
  if (!truck) {
    load.status = 'NEW';
    load.logs.push({message: "Load was rolled back to NEW", time: new Date().toISOString()})
    await load.save();
    return res.status(400).json({ message: 'Truck is not found' });
  }
  assignLoad(truck, load)
  return res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

const getUserActiveLoad = async (req, res) => {
  const load = await Load.findOne({ assigned_to: req.user.userId });
  if (!load) {
    return res.status(400).json({ message: 'Access denied' });
  }
  return res.status(200).json({ load });
};

const iterateLoadState = async (req, res) => {
  const truck = await Truck.findOne({ assigned_to: req.user.userId });
  const load = await Load.findOne({ assigned_to: req.user.userId });
  if (!load) {
    return res.status(400).json({ message: 'Access denied' });
  }
  if (load.state == 'En route to Pick Up') {
    load.state = 'Arrived to Pick Up';
    load.save();
    return res.status(200).json({ message: "Load state changed to 'Arrived to Pick Up'" });
  }
  if (load.state == 'Arrived to Pick Up') {
    load.state = 'En route to Delivery';
    load.save();
    return res.status(200).json({ message: "Load state changed to 'En route to Delivery'" });
  }
  if (load.state == 'En route to Delivery') {
    load.state = 'Arrived to Delivery';
    load.status = 'SHIPPED';
    load.logs.push({message: `Load delivered`, time: new Date().toISOString()})
    truck.status = 'IS';
    load.save();
    truck.save();
    return res.status(200).json({ message: "Load state changed to 'Arrived to Delivery'" });
  }
};

const getUserLoadShippingDetailsById = async (req, res) => {
  const load = await Load.findOne({ _id: req.params.id, created_by: req.user.userId, status: 'ASSIGNED' });
  if (!load) {
    return res.status(400).json({ message: 'Access denied' });
  }
  const truck = await Truck.findOne({ assigned_to: load.assigned_to });
  return res.status(200).json({ load, truck });
};

module.exports = {
  addUserLoad,
  getUserLoadById,
  getUserLoads,
  deleteUserLoadById,
  updateUserLoadById,
  postUserLoadById,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadShippingDetailsById,
};
