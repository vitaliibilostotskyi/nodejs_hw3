const express = require('express');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

const router = express.Router();
const {
  addUserLoad,
  getUserLoadById,
  getUserLoads,
  deleteUserLoadById,
  updateUserLoadById,
  postUserLoadById,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadShippingDetailsById,
} = require('../controllers/loadsController');

router.post('/', authMiddleware, asyncWrapper(addUserLoad));

router.get('/', asyncWrapper(getUserLoads));

router.get('/active', asyncWrapper(getUserActiveLoad));

router.patch('/active/state', asyncWrapper(iterateLoadState));

router.get('/:id', asyncWrapper(getUserLoadById));

router.get('/:id/shipping_info', asyncWrapper(getUserLoadShippingDetailsById));

router.put('/:id', asyncWrapper(updateUserLoadById));

router.delete('/:id', asyncWrapper(deleteUserLoadById));

router.post('/:id/post', asyncWrapper(postUserLoadById));

module.exports = {
  loadsRouter: router,
};
