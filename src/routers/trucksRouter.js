const express = require('express');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

const router = express.Router();
const {
  addUserTruck,
  getUserTruckById,
  deleteUserTruckById,
  getUserTrucks,
  updateUserTruckById,
  assignUserTruckById,
} = require('../controllers/trucksController');

router.post('/', authMiddleware, asyncWrapper(addUserTruck));

router.get('/', asyncWrapper(getUserTrucks));

router.get('/:id', asyncWrapper(getUserTruckById));

router.put('/:id', asyncWrapper(updateUserTruckById));

router.post('/:id/assign', asyncWrapper(assignUserTruckById));

router.delete('/:id', asyncWrapper(deleteUserTruckById));

module.exports = {
  trucksRouter: router,
};
