const { Load } = require('../models/Loads');
const { Truck, truckTypesDem } = require('../models/Trucks');

const saveLoad = async ({
  name, payload, pickup_address, delivery_address, dimensions, req,
}) => {
  const load = new Load({
    created_by: req.user.userId,
    assigned_to: null,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_date: new Date().toISOString(),
  });
  load.logs.push({ message: "Load was created", time: new Date().toISOString() })
  return load.save();
};

const updateLoad = async (
  { name, payload, pickup_address, delivery_address, dimensions, load }
) => {
  await load.updateOne(
    {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      $push: {
        logs: {
          message: 'Load was updated',
          time: new Date().toISOString(),
        },
      },
    }
  )
}

const deleteLoad = (load) => {
  load.deleteOne()
}

const postLoad = async (load) => {
  load.status = 'POSTED';
  load.logs.push({message: "Load posted", time: new Date().toISOString()})
  await load.save();
}

const assignLoad = (truck, load) => {
  load.status = 'ASSIGNED';
  load.state = 'En route to Pick Up';
  load.assigned_to = truck.assigned_to;
  load.logs.push({message: `Load assigned to driver with id ${load.assigned_to}`, time: new Date().toISOString()})
  truck.status = 'OL';
  load.save();
  truck.save();
}

module.exports = {
  saveLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  assignLoad
};
