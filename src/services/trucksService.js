const { Truck } = require('../models/Trucks');

const saveTruck = async (type, req) => {
  const truck = new Truck({
    created_by: req.user.userId,
    assigned_to: null,
    type,
    status: 'IS',
    created_date: new Date().toISOString(),
  });
  return truck.save();
};

module.exports = {
  saveTruck,
};
