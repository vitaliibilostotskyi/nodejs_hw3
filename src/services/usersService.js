const bcrypt = require('bcryptjs');
const { User } = require('../models/Users');

const saveUser = async ({
  name, username, password, role, email,
}) => {
  const user = new User({
    role: role.toUpperCase(),
    name,
    username,
    email,
    password: await bcrypt.hashSync(password, 10),
    created_date: new Date().toISOString(),
  });
  return user.save();
};

const savePass = async (user, newPassword) => {
  user.password = await bcrypt.hash(newPassword, 10);
  return user.save();
};

module.exports = {
  saveUser,
  savePass,
};
